# commit-conventions

## Commit Rules

### Rules

At each commit, the developer shall indicate the type of the performed commit by adding at its start:

```fix:``` for a commit patches a bug in your codebase.

```feat:``` for a commit that introduces a new feature to the codebase.

```misc:``` for a commit of a different type than the present ones.

```test:``` for a commit that only relates to the testing at/of the code base.

```ci:``` for a commit on changes related to CI/CD.

```docs:``` for a commit related to documentation.

```refactor:``` for a commit that refactors the codebase.

Other options:  ```build```, ```chore```, ```style```, ```perf```.

An optional scope can also be provided after the type as, for example, ```feat(api)!:```

### Example:

```git commit -m "feature: new method to parse input strings.```

### Setting the Hook

#### Having the CMake of the project set it

```
configure_file(${CMAKE_CURRENT_SOURCE_DIR}/Scripts/commit-msg ${CMAKE_CURRENT_BINARY_DIR}/../.git/hooks/commit-msg COPYONLY)
```


#### Manually

Under .git/hooks rename the file "commit-msg.sample" to only "commit-msg" and add the following code:

```
#!/bin/bash

MSG="$1"

if grep -qE "^feat.*: " "$MSG";then
    exit 0
elif grep -qE "^fix.*: " "$MSG";then
	exit 0
elif grep -qE "^misc.*: " "$MSG";then
	exit 0
elif grep -qE "^test.*: " "$MSG";then
	exit 0
elif grep -qE "^ci.*: " "$MSG";then
	exit 0
elif grep -qE "^docs.*: " "$MSG";then
	exit 0
elif grep -qE "^refactor.*: " "$MSG";then
	exit 0
elif grep -qE "^build.*: " "$MSG";then
	exit 0
elif grep -qE "^chore.*: " "$MSG";then
	exit 0
elif grep -qE "^style.*: " "$MSG";then
	exit 0
elif grep -qE "^perf.*: " "$MSG";then
	exit 0
fi

cat "$MSG"
echo "ERROR: Your commit message must contain the type of the commit (e.g.: "feat:", "fix:", "misc:", "test:", "ci:", "docs:", "refactor:"."
exit 1
```
